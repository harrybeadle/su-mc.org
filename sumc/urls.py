from django.urls import path

from . import views

app_name = 'sumc'

urlpatterns = [
    path('', views.index, name='index'),
]
